# -*- coding: utf-8 -*-
require 'rubygems'
require 'mysql2'

start_time = Time.now

temp = Array.new

def count_all(th)
  db = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :database => "reco_y01_development")
  db.query('select * from hotel_infos').each do |row|
    sum = 0 
    sum = sum + counter(row["rating1"],th,sum,1)
    sum = sum + counter(row["rating2"],th,sum,1)
    sum = sum + counter(row["rating3"],th,sum,1)
    sum = sum + counter(row["rating4"],th,sum,1)
    sum = sum + counter(row["rating5"],th,sum,1)
    sum = sum + counter(row["rating6"],th,sum,1)
    sum = sum + counter(row["rating7"],th,sum,1)

  end
end


def counter_over(data,th,sum,witch)
  if which == 1
    if data >= th 
      sum = sum +1
    end
  end

  if which == -1
    if data <= th 
      sum = sum +1
    end
  end

  return count
end


def  prefs#レコードを取得→アイテムと評価値データをハッシュとして格納
  item_model = prefs_m#アイテムモデルのデータ

  db = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :database => "reco_y01_development")
  unko2= Hash.new { |hash,key| hash[key] = Hash.new {} }#この宣言の仕方だとvalueを配列宣言しているので①for文で取り出せる②1対多数のハッシュ登録が可能！
  #http://tbpgr.hatenablog.com/entry/20120704/1341427952
  db.query('select * from hotel_ratings').each do |row|
     if item_model.key?(row["item_ID"]) == true&&row["user_ID"] !='anonymous'&&row["rating0"]!=nil
      unko2[row["item_ID"]][row["user_ID"]]=row["rating0"] #item user = rating
      #puts row[0]
      #なんと 10062087=>{"peipan"=>5.0}, 10062087=>{"polochan"=>1.0}
      #じゃなくて 10062087=>{"peipan"=>5.0, "polochan"=>1.0}
      #と同じハッシュ値でまとめてくれるので便利！(宣言でvalueを配列でやってくれてるためらしい)
      end
    end
  db.close
  return unko2
end



def sim_distance(prefs, item1, item2)#類似度計算　コサイン
 shared_persons_a = shared_persons_a(prefs, item1, item2) #item1とitem2の両方に投稿している人を取得
  return 0 if shared_persons_a.length <= 0 #人数がn人以下の場合は類似度
  sum_of_squares =0 
  for person in  shared_persons_a#コサイン
   sum_of_squares = sum_of_squares + (prefs[item1][person]-prefs[item2][person])**2#評価値の差分
  end

  return 1/(1+sum_of_squares)
end

#類似度計算　ピアソン
def sim_pearson(prefs, item1, item2)
  shared_persons_a = shared_persons_a(prefs, item1, item2)

  n = shared_persons_a.size
  return 0 if n == 0
  return 0 if n <= 1

  sum1 = shared_persons_a.inject(0) {|result,si|
    result + prefs[item1][si]
  } 
  sum2 = shared_persons_a.inject(0) {|result,si|
    result + prefs[item2][si]
  } 
  sum1_sq = shared_persons_a.inject(0) {|result,si|
    result + prefs[item1][si]**2
  }
  sum2_sq = shared_persons_a.inject(0) {|result,si|
    result + prefs[item2][si]**2
  } 
  sum_products = shared_persons_a.inject(0) {|result,si|
    result + prefs[item1][si]*prefs[item2][si]
  }

  num = sum_products - (sum1*sum2/n)
  den = Math.sqrt(((sum1_sq - sum1**2/n)*(sum2_sq - sum2**2/n)).abs)
  return 0 if den == 0
  return num/den
end

#類似度計算　ピアソン2
def sim_pearson2(prefs, item1, item2)
  rank = Array.new

  n = 7

  (0..6).each do |i|
    sum1 = (i*4..i*4+3).inject(0) {|result,si|
      result + prefs[item1][si]
    } 
    sum2 = (i*4..i*4+3).inject(0) {|result,si|
      result + prefs[item2][si]
    } 
    sum1_sq = (i*4..i*4+3).inject(0) {|result,si|
      result + prefs[item1][si]**2
    }
    sum2_sq = (i*4..i*4+3).inject(0) {|result,si|
      result + prefs[item2][si]**2
    } 
    sum_products = (i*4..i*4+3).inject(0) {|result,si|
      result + prefs[item1][si]*prefs[item2][si]
    }

    num = sum_products - (sum1*sum2/n)
    den = Math.sqrt(((sum1_sq - sum1**2/n)*(sum2_sq - sum2**2/n)))
    if den == 0
      temp = 0
    else
      temp = num/den
    end
    rank << [temp.abs,i]
  end
  rank.sort.reverse
  

end


def shared_persons_a(prefs, item1, item2)#item1とitem2の両方に投稿している人を取得
   return prefs[item1].keys&prefs[item2].keys
end




# def top_matches(prefs, item, n=10)#類似度計算オーダー
#   scores = Array.new
#   prefs.each do |key,value| #ハッシュのeach   keyはvalue "{"yoskt"=>2.5, "kibidango"=>4.0, "mj3003"=>3.5, "zakkiku"=>1.0}"
#     if key != item
#       next if  shared_persons_a(prefs, item, key).length <= 1 #評価されているレビュー数の制限　MSWさんによると２件以上必須
#       # 類似度算出アルゴリズムのメソッドにアイテム名と評価ユーザ、スコアを送信する
#     scores << [__send__(:sim_distance,prefs,item,key),key]#[__send__(メソッド、引数),key]→[戻り値(類似度),key]
#     end
#   end
# return scores.sort { |a,b| a[0] <=> b[0]}.reverse#.reverse[0,n]   # スコアの降順にソートして返却
# end



#person の評価したアイテム以外の全アイテムの評点の重み付き平均を使い person への推薦を算出する
def get_recommendations(prefs, person, similarity=:sim_pearson)
  #item ゆー座が評価したやつ
  rankings = Array.new

  prefs.each_key do |other| #各アイテムごと
    totals = 0
    sim_sums = 0
    sim_counts = 0

    
    person.each do |item,val| #各ユーザ評価アイテムごと
      next if other == item
      sim = __send__(similarity,prefs,item,other)#ユーザ評価アイテムと他の類似度　本当はまとめて事前計算しとけぇ…
      sim = sim.abs
      totals+= sim*val
      sim_sums += sim
      next if sim == 0 #counts ここの２行で下のcounts発動
      sim_counts +=1
    end

    next if sim_sums == 0
    next if sim_counts <= 1 #counts ここで評価したアイテムと類似度が計算できたアイテムの数を調整 1だとそのアイテムにすごく影響される
   rankings << [totals/sim_sums, other]
  end

  rankings.sort.reverse#[0,20]
end



#user_profile===============================================

person = { 
  324031=>2,345620=>4,344125=>3,347378=>1,348235=>2, 
  335872=>3,326744=>1,321423=>3,323212=>1,9467=>5,
  159321=>3,149868=>1,324899=>3,327384=>1,327253=>5,
  344581=>3,327253=>1,344976=>3,348899=>1,344130=>5 
}

person = { 
  10218907=>2,10052713=>4,10057625=>3,10051789=>5,10051865=>2, 
  10051878=>3,10051788=>1,10058451=>3,10051799=>1,10034414=>5,
  10051823=>3,10038347=>5,10058478=>3,10669336=>5,10051800=>5,
  10220406=>3,10053636=>1,11288158=>3,11187532=>1,10053418=>5 
}
#config========================
#・アイテム比較に必要なレビュー数
#・重み付けに使用するレビュー数の下限（少ないとそのアイテムだけの影響を受ける）
#・類似度の計算方法：:cosin :peason
#・推薦方法（何を比較するか？）従来法アイテムベースCF:prefs   提案手法:prefs_m


#main_=================================================
#念のため：ユーザが評価したアイテムでモデルができているやつのみ(実験では２０件全部に当てはめるつもり)
person_a ={}
(person.keys&prefs_m.keys).each do |item|
  person_a[item] = person[item]
end



p get_recommendations(prefs ,person_a)
# p shared_persons_a(prefs_m, 10036427, 10047951)
#  p prefs_m[10036427]
#  p prefs_m[10047951]
# p sim = __send__(:sim_pearson,prefs,10035560,10049831)













# #類似度一覧計算=================================================
# def get_sim(prefs)
#   sim_all = Array.new
#   prefs.each_key do |item1| #各アイテムごと  
#     prefs.each_key do |item2| #各ユーザ評価アイテムごと
#         next if item1 == item2
#         sim = __send__(:sim_pearson,prefs,item1,item2)#ユーザ評価アイテムと他の類似度　本当はまとめて事前計算しとけぇ…

#         sim_all << ((sim.abs)*10).floor
#     end
#   end
#   sim_all
# end


# sims_all = Array.new
# sims_count = Array.new
# sims_all = get_sim(prefs)

# (0..10).each do |i|
#   sims_count << sims_all.count(i).to_f/sims_all.size*100
# end

#  p sims_count


#=================================================

p "処理概要 #{Time.now - start_time}s"



#memo=================================================
#注意点
#puts top_matches(prefs,  10220494)#!必ず数値指定 '数字'だと文字列扱いで、対応するkeyを自動生成しようとしてエラーがおこる


#まとめ：仕様

  #二次元配列を使用
    # 10062087=>{"peipan"=>5.0, "polochan"=>1.0}
    # key: 10062087
    # values:  {"peipan"=>5.0, "polochan"=>1.0}
    # xx[key].keys      'peipan','polochan'

  #prefはレビューデータ、pref_mはモデリングデータ
    #prefは{item=>{"user"=>val}} , pref_mは{item=>{"属性*リフト値の種類(28個)"=>val}}
    #これによりprefはIBCFによる類似度、pref_mはリフト値による類似度推薦が可能





