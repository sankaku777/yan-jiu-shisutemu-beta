#極性判定を作成するプログラム

# -*- coding: utf-8 -*-
require 'rubygems'
require 'mysql2'
require 'csv'

start_time = Time.now


def  prefs_m#レコードを取得→アイテムとモデルデータをハッシュとして格納
  db = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :database => "reco_y01_development")

  unko2= Hash.new { |hash,key| hash[key] = Hash.new {} }  
  i = 0
  db.query('select * from hotel_models').each do |row| 
    i = i%7
    unko2[row["item_ID"]][i*4+0]=row["lift_oo"] #item 属性と４(4*n) = model_val
    unko2[row["item_ID"]][i*4+1]=row["lift_ox"] 
    unko2[row["item_ID"]][i*4+2]=row["lift_xo"] 
    unko2[row["item_ID"]][i*4+3]=row["lift_xx"] 
    i = i+1
  end
  db.close
  return unko2
end


def  prefs(propaty)#レコードを取得→アイテムと評価値データをハッシュとして格納
  item_model = prefs_m#アイテムモデルのデータ


  db = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :database => "reco_y01_development")
  unko2= Hash.new { |hash,key| hash[key] = Hash.new {} }#この宣言の仕方だとvalueを配列宣言しているので①for文で取り出せる②1対多数のハッシュ登録が可能！
  #http://tbpgr.hatenablog.com/entry/20120704/1341427952


  db.query('select * from hotel_ratings').each do |row|
     if item_model.key?(row["item_ID"]) == true&&row["user_ID"] !='anonymous'&&row[propaty]!=nil

      unko2[row["item_ID"]][row["user_ID"]]=row[propaty] #item user = rating
      end
    end
  db.close
  # return temp
  return unko2
end


def total_rs
  item_model = prefs_m
  db = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :database => "reco_y01_development")
  data = Hash.new{}

  db.query('select * from hotel_infos').each do |row|
      data[row["item_ID"]]=row["rating0"] #item user = rating
    end
  db.close

  return data
end







def c_pl(polarity_all,propaty,type)
  datas = prefs(propaty)

  total_r = Hash.new{}
  total_r = total_rs  

  pall_data = Array.new
  # hotel_infos = CSV.read('hotel_infos.csv')

  

  prefs(propaty).keys.each do |row|
    p_data = Array.new
    data = datas[row].values

    # 方法0 3を基準
    # th = 3

    # 方法１ 各アイテムの各属性の平均値
    ave =  data.inject(0.0){|r,i| r+=i }/data.size
    th = ave

    # 方法２ 各アイテムの総合評価値の平均値
    # th = total_r[row]

    # 方法3 全アイテムの総合評価値の最頻値
    # th = 4

    #各極性の数を数える
    p_data << data.count{|i| i >= th+0.25}
    p_data << data.count{|i| i > th-0.25 && i < th+0.25}
    p_data << data.count{|i| i <= th-0.25}

    

    if p_data.index(p_data.max) == 0
      polarity = "pos"
    end

    if p_data.index(p_data.max) == 1
      polarity = "mid"
    end

    if p_data.index(p_data.max) == 2
      polarity = "neg"
    end

    #一番多い極性を代入
    if polarity_all[row][polarity] == nil
      temp = Array.new
      temp << type
      polarity_all[row][polarity] = temp.join(",")
    else
      temp = polarity_all[row][polarity].split(",")
      temp << type
      polarity_all[row][polarity] = temp.join(",")
    end

  end
    # hotel_infos[(hotel_infos.transpose)[0].find_index{ |n| n == row.to_s }]
end


db = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :database => "reco_y01_development")

data = Hash.new { |hash,key| hash[key] = Hash.new {} } 
db.query('select * from hotel_infos').each do |row|
  row.each do |key, value|
   data[row["item_ID"]][key]= value
  end
end
db.close


p prefs("rating7").keys - data.keys).size

# c_pl(data,"rating1",1)
# c_pl(data,"rating2",2)
# c_pl(data,"rating3",3)
# c_pl(data,"rating4",4)
# c_pl(data,"rating5",5)
# c_pl(data,"rating6",6)
# c_pl(data,"rating7",7)


# data.keys.each do |row|
#   temp = Array.new
#   temp << data[row]["item_ID"]
#   temp << data[row]["item_Name"]
#   temp << data[row]["rating0"]
#   temp << data[row]["rating1"]
#   temp << data[row]["rating2"]
#   temp << data[row]["rating3"]
#   temp << data[row]["rating4"]
#   temp << data[row]["rating5"]
#   temp << data[row]["rating6"]
#   temp << data[row]["rating7"]
#   temp << data[row]["pos"]
#   temp << data[row]["mid"]
#   temp << data[row]["neg"]
#   temp << data[row]["item_type"]
#   temp << data[row]["intro"]
#   temp << data[row]["address"]
#   temp << data[row]["access"]
#   temp << data[row]["detail"]
#   temp << data[row]["thumbnail"]
#   CSV.open("test.csv","a") do |csv|
#     csv << temp
#   end
# end






# p data
# temp = Array.new
# temp << 1
# p temp.join(",")
  # p [c_pl("rating1"),c_pl("rating2"),c_pl("rating3"),c_pl("rating4"),c_pl("rating5"),c_pl("rating6"),c_pl("rating7")].transpose.map{|ary| ary.inject(&:+)} 



#=================================================

p "処理概要 #{Time.now - start_time}s"



#memo=================================================
#注意点
#puts top_matches(prefs,  10220494)#!必ず数値指定 '数字'だと文字列扱いで、対応するkeyを自動生成しようとしてエラーがおこる


#まとめ：仕様

  #二次元配列を使用
    # 10062087=>{"peipan"=>5.0, "polochan"=>1.0}
    # key: 10062087
    # values:  {"peipan"=>5.0, "polochan"=>1.0}
    # xx[key].keys      'peipan','polochan'

  #prefはレビューデータ、pref_mはモデリングデータ
    #prefは{item=>{"user"=>val}} , pref_mは{item=>{"属性*リフト値の種類(28個)"=>val}}
    #これによりprefはIBCFによる類似度、pref_mはリフト値による類似度推薦が可能





