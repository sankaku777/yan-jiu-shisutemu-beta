
#ホテルのサムネ画像を保存するクローラー
#実際使用するホテルIDを参照して(title_lists.csv)行う


# URLにアクセスするためのライブラリの読み込み
require 'open-uri'

# Nokogiriライブラリの読み込み
require 'nokogiri'

require 'csv'

#ホテルIDを元にホテル情報を取得
def get_info(hotel_id)
    # スクレイピング先のURL
  url = 'http://4travel.jp/dm_hotel_each-'+(hotel_id).to_s+'.html'

  charset = nil
  html = open(url) do |f|
    charset = f.charset # 文字種別を取得
    f.read # htmlを読み込んで変数htmlに渡す
  end

  # htmlをパース(解析)してオブジェクトを生成
  doc = Nokogiri::HTML.parse(html, nil, charset)
  item_info = Array.new

  item_info[0] = hotel_id

  #title
  item_info[1] = doc.xpath("//h1[@class='u_title']").text

  #rating0
  item_info[2] = doc.xpath("//span[contains(@class,'satisfactionStar')]").text.to_f

  #rating0
  item_info[3] = doc.xpath("//ul[@class='u_tipsEvalList']").xpath("//span[@class='number']")[0].text.to_f
  item_info[4] = doc.xpath("//ul[@class='u_tipsEvalList']").xpath("//span[@class='number']")[1].text.to_f
  item_info[5] = doc.xpath("//ul[@class='u_tipsEvalList']").xpath("//span[@class='number']")[2].text.to_f
  item_info[6] = doc.xpath("//ul[@class='u_tipsEvalList']").xpath("//span[@class='number']")[3].text.to_f
  item_info[7] = doc.xpath("//ul[@class='u_tipsEvalList']").xpath("//span[@class='number']")[4].text.to_f
  item_info[8] = doc.xpath("//ul[@class='u_tipsEvalList']").xpath("//span[@class='number']")[5].text.to_f
  item_info[9] = doc.xpath("//ul[@class='u_tipsEvalList']").xpath("//span[@class='number']")[6].text.to_f

  #ホテルのジャンル
  item_info[10] = doc.xpath("//span[@class='ico_category']").text

  #ホテル説明
  item_info[11] = doc.xpath("//p[@class='l_spotTagLineText']").text

  #住所
  item_info[12] = doc.xpath("//table[@class='u_shisetsuEachAdressTable']/tbody/tr[1]/td").text
  #access
  item_info[13] = doc.xpath("//table[@class='u_shisetsuEachAdressTable']/tbody/tr[2]/td").text

  #設備項目
  item_info[14] = doc.xpath("//table[@class='l_facilitiesTable']").inner_html.delete("\n").delete("\\")
  item_info[15] = '<table>'+item_info[13]+'</table>'

   #サムネ画像
  item_info[16] = doc.xpath("//div[@class='main_photo_area photoMWrap'][1]").css('img').attribute('src').value



  item_info

end





#画像urlとホテルIDを元に画像を保存
def save_file(url,hotel_id)
  filename = File.basename(url)

  dirName = "/Users/yammar/desktop/images/"
  filename = dirName + hotel_id.to_s + '.jpg'

  open(filename,'wb') do |file|
    open(url) do |data|
      file.write(data.read)
    end
  end

end

i=0;
hotels = Array.new

CSV.foreach("title_lists.csv") do |row|
  hotels << row
end
p 'done'



CSV.open("hotel_info.csv","wb") do |csv|
  hotels.each do |row|
    hotel_id = row[0]
    p info = get_info(hotel_id)
    # save_file(info[14],hotel_id)
    p i = i+1
    p hotel_id
    temp = 1 + (1+rand(1..20).to_f/10.0)
    sleep(temp)
    csv << info
  end
end





