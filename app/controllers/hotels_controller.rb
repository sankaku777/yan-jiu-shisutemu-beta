
class HotelsController < ApplicationController
	  
	#課題,rating_type の確認整理、testとrecoのかぶり、

	#フォーム関連＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	def permitted_params#ハッシュの受け取り許可		
		params[:user_rating]
		params.require(:user_rating).permit(:item_ID, :user_ID,:rating_type, :rating0,:r_point_pos,:r_point_mid,:r_point_neg,:r_point_oo,:r_point_xo,:r_point_xx,:r_propaty_pos,:r_propaty_mid,:r_propaty_neg,:r_propaty_oo,:r_propaty_xo,:r_propaty_xx,:r_reason_pos,:r_reason_mid,:r_reason_neg,:r_reason_oo,:r_reason_xo,:r_reason_xx,:rating_reason,:comment)
	end

	def create
		#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase

		#テストアイテム情報のフォーム受け取り
		#rating_type(012)で：一覧へのリンクは：テストアイテム：推薦アイテム：どちら？
		if  params[:user_rating][:rating_type] == "0"
			@page_type = 'list_test'
		else 
			@page_type = 'list_recommend'
		end


		#受け取ったパラメータのアイテムIDと一致するユーザアイテムのモデルを取得
		request_item = UserRating.where(:user_ID => current_user.email).where(:item_ID =>params[:user_rating][:item_ID])
		

		# モデルが２つ以上あるときの処理＝推薦アイテムが重複[MISSION02:kaburi]
		# 手法１：アイテム数が２個以上→被ってるやん検出法
		if request_item.pluck(:item_ID).length >=2 && @phase != 1

			#かぶりあり
			#各場合（従来法、提案法）にそれぞれ追加更新
			params[:user_rating][:rating_type] = 1
			unless request_item.find_by(:rating_type => 1).update(permitted_params)
				@errors = true
			end
			#MISSION 万が一空の時はエラー　初期化必要
			
			params[:user_rating][:rating_type] = 2
			unless request_item.find_by(:rating_type => 2).update(permitted_params)
				@errors = true
			end
		else
			#かぶりなし
			#それ以外の場合、１こしかないのでそのまま追加更新
			unless request_item.find_by(:rating_type => params[:user_rating][:rating_type]).update(permitted_params)
				@errors = true
			end
    	end



		if @phase <=1
			#ユーザプロファイルよりテストアイテムを引き出す
			@hotels = HotelInfo.where(:item_ID => UserProfile.find_by(:user_ID=> current_user.email).item01.split(","))
			
			#テストアイテムの投稿データ（レコード）を引き出す
			@user_rated= UserRating.where(:user_ID => current_user.email).where(:rating_type => 0)
			#nil要素はないよね？nilがなくなるとtureを返す。（本来のblankとは逆の働きだが、レコードだと[nil,nil]もblank判定しないので・・・）
			@check_rated = @user_rated.where(:rating_type =>0).where(:rating0 => nil).blank?
		else
			#ユーザの投稿した推薦アイテムの評価値レコード
			@user_rated = UserRating.where(:user_ID => current_user.email).where.not(:rating_type =>0)
			@check_rated = @user_rated.where(:rating0 => nil).blank?
			#推薦アイテムがかぶっている場合、重複を除く[MISSION:kaburi02]
			temp=@user_rated.pluck(:item_ID).uniq#uniq なしだとかぶる
			#推薦アイテム一覧を取得
			@recommend_items = HotelInfo.where(:item_ID => temp)

			#推薦アイテムに対して空の評価が一つもなければ=全て評価すれば
			if  @check_rated
				#段階が2だったら段階3(アンケート)へ
				UserProfile.find_by(:user_ID => current_user.email).update(:phase => 3) if  @phase ==2
			end


		end

		#現在のフェーズ更新
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase
		@rating_info1 = HotelInfo.find_by(:item_ID =>params[:user_rating][:item_ID])
		@rating_info2 = UserRating.where(:user_ID => current_user.email).find_by(:item_ID =>params[:user_rating][:item_ID])

	
	end

	def ask_recommend
		#現在のフェーズ取得
    	@phase = UserProfile.find_by(:user_ID => current_user.email).phase

	end

	#view関連＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	def index

		#ユーザプロファイルげっちゅ
		user = UserProfile.find_by(:user_ID=>current_user.email)

		
		#ユーザIDが存在しなければ、ユーザプロファイル作成
		if user.blank? 
			#ホテルはアイテムモデルを持つやつのみランダムで引っ張ってくる[MISSION:hani]
			test_item = HotelTitle.where(:item_ID => HotelModel.pluck(:item_ID).uniq.shuffle[0..$n_test_item]).pluck(:item_ID)
			UserProfile.create(user_ID: current_user.email, phase: 1, item01: test_item.join(", "))

			#ユーザの投稿アイテムの方にも初期登録(デリート処理かアップデートどっち？)
			test_item.each do |hotel|
				UserRating.create(item_ID: hotel, user_ID: current_user.email ,rating_type: 0)
			end	
		end


		#ユーザプロファイル更新
		user = UserProfile.find_by(:user_ID=>current_user.email)

		#フェーズ更新
		sleep(1)
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase
	end

	def manual
		#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase

	end
		

	def list_test
		#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase

		#ユーザプロファイルよりテストアイテムを引き出す
		@hotels = HotelInfo.where(:item_ID => (UserProfile.find_by(:user_ID=> current_user.email).item01.split(",")))
		#テストアイテムの投稿データ（レコード）を引き出す
		@user_rated= UserRating.where(:user_ID => current_user.email).where(:rating_type => 0)
		#nil要素はないよね？nilがなくなるとtureを返す。（本来のblankとは逆の働きだが、レコードだと[nil,nil]もblank判定しないので・・・）
		@check_rated = @user_rated.where(:rating0 => nil).blank?



	end

	def list_recommend
		#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase

		#推薦アイテムを作成していないなら作成して、phase 2に移行
		if @phase <=1
			UserProfile.find_by(:user_ID => current_user.email).update(:phase => 2)

		#ユーザのテストアイテムの評価レコード取得→推薦アイテム作成の引数に使う
		user_rated = (UserRating.where(:user_ID => current_user.email).where(:rating_type =>0).where.not(:rating0 => nil))#where.not(:rating0 => nil))はcheck_ratedで確認してるからいらないけど一応
		user_profile = Hash.new
		user_rated.each do |item|
			user_profile[item.item_ID] = item.rating0
		end

			@reco_cbf= get_recommendations(prefs,user_profile).transpose
			@reco_pim= get_recommendations(prefs_m,user_profile).transpose
			# 従来手法と提案手法で被ってるアイテムリスト
			same_reco = @reco_cbf[1]&@reco_pim[1]

			#UPDATEでよくない？ MSSION
			UserRating.where(user_ID: current_user.email).where(rating_type: 1).destroy_all#一旦全消去
			UserRating.where(user_ID: current_user.email).where(rating_type: 2).destroy_all#一旦全消去
			HotelTitle.where(:item_ID => @reco_cbf[1]).each do |hotel|
				UserRating.create(item_ID: hotel.item_ID, user_ID: current_user.email ,rating_type: 1)
			end
			HotelTitle.where(:item_ID => @reco_pim[1]).each do |hotel|
				UserRating.create(item_ID: hotel.item_ID, user_ID: current_user.email ,rating_type: 2)
			end

			#phase2に移行 ＋プロファイルに推薦アイテムを記録
			UserProfile.find_by(:user_ID => current_user.email).update(:phase => 2,:memo02=> same_reco.join(", "),:item02 =>@reco_cbf[1].join(", "), :item03=>@reco_pim[1].join(", "))
	
		end



		#ユーザの投稿した推薦アイテムの評価値レコード
		@user_rated = UserRating.where(:user_ID => current_user.email).where.not(:rating_type =>0)
		
		#推薦アイテムがかぶっている場合、重複を除く[MISSION:kaburi02]
		temp=@user_rated.pluck(:item_ID).uniq#uniq なしだとかぶる
		#推薦アイテム一覧を取得
		@recommend_items = HotelInfo.where(:item_ID => temp)

		#フェーズ更新
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase



		# #かぶり確認デバッグ
		# a = UserRating.where(:user_ID => current_user.email).where(:rating_type =>1).pluck(:item_ID)
		# b = UserRating.where(:user_ID => current_user.email).where(:rating_type =>2).pluck(:item_ID)
		# @a = a&b
		# @b = @recommend_items.size

	end


	def show
		@post =	UserRating.new

		#リクエストされたホテルID取得
		hotel_id = params[:item_ID]	
		#ホテルIDのタイトル取得
		@hotel = HotelTitle.find_by(:item_ID => hotel_id)

		#テストアイテムか推薦アイテムか
		@page_type = params[:type]

		#テストアイテム、従来法、提案法のどれか
		@rating_type = UserRating.where(:user_ID => current_user.email).where(:item_ID =>hotel_id).pluck(:rating_type)

		#temp
		@hotel_info= HotelInfo.find_by(:item_ID => hotel_id)
		
		#平均評価値表示
		@rating = get_ratings(hotel_id)

		#参考理由の誘導文章
		@rating_reason = "その理由↓"

		#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase	



		###推薦文章作成--------------------
		hotel_m= HotelModel.where(:item_ID => hotel_id)#指定ホテルIDのアイテムモデル取得

		#評価値について、閾値に応じた属性番号（１〜７）を取得
		#空（存在しない）なら0にして0.png画像表示
		#t,m,b top,middle,bottom
		if @hotel_info.pos.present?
			@rating_pos = @hotel_info.pos.split(',')
		else
			@rating_pos= [0]
		end

		if @hotel_info.mid.present?
			@rating_mid = @hotel_info.mid.split(',')
		else
			@rating_mid= [0]
		end

		if @hotel_info.neg.present?
			@rating_neg = @hotel_info.neg.split(',')
		else
			@rating_neg= [0]
		end



		 


		#リフト値が閾値を超える属性番号[MISSION平均値求める]（１〜７）を取得
		#1.35 1.2 1.9 (1300) or 1.25 1.1 1.65 (2000)
		@top_oo = hotel_m.where("lift_oo >=  ?",1.2).pluck(:propaty)
			@top_oo= [0] if@top_oo.blank?
		# @top_ox = hotel_m.where("lift_ox >=  ?",1.25).pluck(:propaty)
		@top_xo = hotel_m.where("lift_xo >=  ?",1.05).pluck(:propaty)
			@top_xo= [0] if@top_xo.blank? 
		@top_xx = hotel_m.where("lift_xx >=  ?",1.5).pluck(:propaty)
			@top_xx= [0] if@top_xx.blank? 
		###-------------------------------

	end

	def result_show
		@post =	UserRating.new

		#リクエストされたホテルID取得
		hotel_id = params[:item_ID]	
		#ホテルIDのタイトル取得
		@hotel = HotelInfo.find_by(:item_ID => hotel_id)

		#テストアイテムか推薦アイテムか
		@page_type = params[:type]

		#temp
		@hotel_info= HotelInfo.find_by(:item_ID => hotel_id)
		
		#平均評価値表示
		@rating = get_ratings(hotel_id)

		#参考理由の誘導文章
		@rating_reason = "その理由↓"


		#自分の評価した情報
		@rating_info = UserRating.where(:user_ID => current_user.email).find_by(:item_ID =>hotel_id)




		###推薦文章作成--------------------
		hotel_m= HotelModel.where(:item_ID => hotel_id)#指定ホテルIDのアイテムモデル取得

		#評価値について、閾値に応じた属性番号（１〜７）を取得
		#空（存在しない）なら0にして0.png画像表示
		#t,m,b top,middle,bottom
		if @hotel_info.pos.present?
			@rating_pos = @hotel_info.pos.split(',')
		else
			@rating_pos= [0]
		end

		if @hotel_info.mid.present?
			@rating_mid = @hotel_info.mid.split(',')
		else
			@rating_mid= [0]
		end

		if @hotel_info.neg.present?
			@rating_neg = @hotel_info.neg.split(',')
		else
			@rating_neg= [0]
		end



		 


		#リフト値が閾値を超える属性番号[MISSION平均値求める]（１〜７）を取得
		#1.35 1.2 1.9 (1300) or 1.25 1.1 1.65 (2000)
		@top_oo = hotel_m.where("lift_oo >=  ?",1.2).pluck(:propaty)
			@top_oo= [0] if@top_oo.blank?
		# @top_ox = hotel_m.where("lift_ox >=  ?",1.25).pluck(:propaty)
		@top_xo = hotel_m.where("lift_xo >=  ?",1.05).pluck(:propaty)
			@top_xo= [0] if@top_xo.blank? 
		@top_xx = hotel_m.where("lift_xx >=  ?",1.5).pluck(:propaty)
			@top_xx= [0] if@top_xx.blank? 
		###-------------------------------

				#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase


	end


	def result
		
		#アンケート終了から遷移した？（アンケート投稿完了ボタンを押してきたらパラメータを取得）
		user = UserProfile.find_by(:user_ID=>current_user.email)
		if params[:user_profile]!=nil
			#ユーザプロファイルのmemo01を１にする
			user.memo01 = params[:user_profile][:memo01]
			user.save
		end

		#推薦アイテム評価終了かつアンケート終了？　フェーズ→４：終わり
		if user.phase == 3 && user.memo01 == "interview_done"
			user.phase = 4
			user.save
		end 


		#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase


		#ユーザプロファイルよりテストアイテムを引き出す
		@hotels = HotelInfo.where(:item_ID => UserProfile.find_by(:user_ID=> current_user.email).item01.split(","))
		#テストアイテムの投稿データ（レコード）を引き出す
		@user_rated01= UserRating.where(:user_ID => current_user.email).where(:rating_type => 0)
		#nil要素はないよね？nilがなくなるとtureを返す。（本来のblankとは逆の働きだが、レコードだと[nil,nil]もblank判定しないので・・・）
		@check_rated = @user_rated01.where(:rating0 => nil).blank?


		#ユーザの投稿した推薦アイテムの評価値レコード
		@user_rated02 = UserRating.where(:user_ID => current_user.email).where.not(:rating_type =>0)
		
		#推薦アイテムがかぶっている場合、重複を除く[MISSION:kaburi02]
		temp=@user_rated02.pluck(:item_ID).uniq#uniq なしだとかぶる
		#推薦アイテム一覧を取得
		@recommend_items = HotelInfo.where(:item_ID => temp)

		@rated_item = UserRating.where(:user_ID => current_user.email)
		
	end

	def interview
		#現在のフェーズ取得
		@phase = UserProfile.find_by(:user_ID => current_user.email).phase
		@post =	UserProfile.new
		
	end


end







#解決済み
#・１、推薦に時間がかかる時→並列処理する→でもブラウザバックとか繰り返すとエラー→もう余計なことはせず、まってもらう
#・[MISSON:kaburi01]、評価アイテムと推薦アイテムのかぶり（get_recommendation のnext if person_a.include?(other)）
#️・[MISSION:hani],初期データベース　モデル側に制限 (pref でpref_mにないやつは除外)
#・hotel?titlesのかぶり→データベース修正
##・$グローバル変数→ユーザプロファイル作成
#・configでクローバル変数設定
#・[MISSION:form] フォーム、参考箇所の送信方法　json 各リフトはもうそれぞれ用意する　
#・[MISSION:form2] [MISSION:form]のランダム提示、属性提示、説明文増加

