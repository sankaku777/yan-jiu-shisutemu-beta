
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :
#  protect_from_forgery 
  #helper_method :text_print

#メモ書き
  #前提として、HotelRating, HotelTiltle はモデルが存在するやつのみにするべし（モデルを使う提案手法と同じ範囲にする）、1394個ある　MISSION hani
  
#phase
# 1 登録+1.5 テスする
# 2 テス評価全て終える＋推薦する
# 3 推薦評価全て終える
# 4 アンケート終える

  #ここにグローバル変数　推薦数とか変数名とか
  #テストアイテムの数
  $n_test_item = 15
  #推薦するアイテムの数
  $n_reco_item = 20








  #==========================
  $n_test_item = $n_test_item -1
  $n_reco_item = $n_reco_item/2

before_action :authenticate_user!

def index
end


# def get_info(hotel_id)
#   require 'open-uri'
#   # URLにアクセスするためのライブラリの読み込み

#   # Nokogiriライブラリの読み込み
#   #require 'nokogiri'

#   # スクレイピング先のURL
#   url = 'http://4travel.jp/dm_hotel_each-'+hotel_id+'.html'

#   charset = nil
#   html = open(url) do |f|
#     charset = f.charset # 文字種別を取得
#     f.read # htmlを読み込んで変数htmlに渡す
#   end

#   # htmlをパース(解析)してオブジェクトを生成
#   doc = Nokogiri::HTML.parse(html, nil, charset)
#   item_info = Array.new
#   #サムネ画像
#   item_info[0] = doc.xpath("//div[@class='main_photo_area photoMWrap'][1]").css('img').attribute('src').value
#   #ホテル説明
#   item_info[1] = doc.xpath("//p[@class='l_spotTagLineText']").text
  
#   #ホテルのジャンル
#   item_info[2] = doc.xpath("//span[@class='ico_category']").text

#   #住所
#   item_info[3] = doc.xpath("//table[@class='u_shisetsuEachAdressTable']/tbody/tr[1]/td").text
#   #access
#   item_info[4] = doc.xpath("//table[@class='u_shisetsuEachAdressTable']/tbody/tr[2]/td").text

#   #設備項目
#   item_info[5] = doc.xpath("//table[@class='l_facilitiesTable']").inner_html.delete("\n").delete("\\")
#   item_info[5] = '<table>'+item_info[5]+'</table>'

#   item_info

# end


#モデルのデータはテーブルで取得→クラスなので xx.カラム名 xx[:カラム名] で単体として引き出せる
#また１つ１つのテーブルはeachで引き出せる
#またカラムをpluckで配列としても引き出せる


#基本情報作成====================================================

#評価平均値作成
def get_ratings(hotel_id)
  hotel_rating = HotelRating.where(:item_ID => hotel_id)
  rating = Array.new
  rating[0] = hotel_rating.average(:rating0).to_f
  rating[1] = hotel_rating.average(:rating1).to_f
  rating[2] = hotel_rating.average(:rating2).to_f
  rating[3] = hotel_rating.average(:rating3).to_f
  rating[4] = hotel_rating.average(:rating4).to_f
  rating[5] = hotel_rating.average(:rating5).to_f
  rating[6] = hotel_rating.average(:rating6).to_f
  rating[7] = hotel_rating.average(:rating7).to_f
  return  rating
end






 #推薦作成======================================================

 #config========================
#・アイテム比較に必要なレビュー数
#・重み付けに使用するレビュー数の下限（少ないとそのアイテムだけの影響を受ける）
#・類似度の計算方法：:cosin :peason
#・推薦方法（何を比較するか？）従来法アイテムベースCF:prefs   提案手法:prefs_m


#モデリング結果の全レコード取得→アイテムとモデルデータをハッシュとして格納　prefs_m[item_ID][lift pattern(4)] = liff_val
def  prefs_m
  unko2= Hash.new { |hash,key| hash[key] = Hash.new {} }  
  i = 0
  HotelModel.pluck(:item_ID, :propaty, :lift_oo, :lift_ox, :lift_xo, :lift_xx).each do |row| #モデリング結果よりアイテム、属性に対応する４つのリフト値を全取得
    i = i%7
      unko2[row[0]][i*3+0]=row[2] #item 属性と４(4*n) = model_val
      # unko2[row[0]][i*4+1]=row[3] 今回はlift_ox削除
      unko2[row[0]][i*3+1]=row[4] 
      unko2[row[0]][i*3+2]=row[5] 
    i = i+1
  end
  return unko2
end


#評価値の全レコードを取得→アイテムと評価値データをハッシュとして格納 prefs[item_ID][user_ID] = rating_val
 def  prefs
  item_model = prefs_m#アイテムモデルのデータ
  unko= Hash.new { |hash,key| hash[key] = Hash.new {} }#この宣言の仕方だとvalueを配列宣言しているので①for文で取り出せる②1対多数のハッシュ登録が可能！
  #http://tbpgr.hatenablog.com/entry/20120704/1341427952
    
    HotelRating.pluck(:item_ID, :user_ID, :rating0).each do |row| #全レコード取得（はじめに配列として入れておく）方法
      
       #取得したアイテムがアイテムモデルを持たない場合、除外[MISSION:hani]
       #+ユーザ名が匿名の場合、無視
       #+評価値がnilの場合、無視
      if item_model.key?(row[0]) == true && row[1] !='anonymous' && row[2]!=nil
        unko[ row[0]  ][ row[1] ]=row[2]#アイテム{ユーザ=>評価値} の２次元ハッシュで格納
      end
    end

  return unko
end




#類似度計算　ピアソン
def sim_pearson(prefs, item1, item2)
  shared_persons_a = shared_persons_a(prefs, item1, item2)

  n = shared_persons_a.size
  return 0 if n == 0
  return 0 if n <= 2#３件以上ペクトルがあわないと類似度x

  sum1 = shared_persons_a.inject(0) {|result,si|
    result + prefs[item1][si]
  } 
  sum2 = shared_persons_a.inject(0) {|result,si|
    result + prefs[item2][si]
  } 
  sum1_sq = shared_persons_a.inject(0) {|result,si|
    result + prefs[item1][si]**2
  }
  sum2_sq = shared_persons_a.inject(0) {|result,si|
    result + prefs[item2][si]**2
  } 
  sum_products = shared_persons_a.inject(0) {|result,si|
    result + prefs[item1][si]*prefs[item2][si]
  }

  num = sum_products - (sum1*sum2/n)
  den = Math.sqrt((sum1_sq - sum1**2/n)*(sum2_sq - sum2**2/n))
  return 0 if den == 0
  return num/den
end



#item1とitem2の両方に投稿している人を取得
def shared_persons_a(prefs, item1, item2)
   return prefs[item1].keys&prefs[item2].keys
end


#person の評価したアイテム以外の全アイテムの評点の重み付き平均を使い person への推薦を算出する
def get_recommendations(prefs, person, similarity=:sim_pearson)


  #念のため：ユーザが評価したアイテムでモデルができているやつのみ(実験では２０件全部に当てはめるつもり)
  person_a ={}
  (person.keys&prefs_m.keys).each do |item|
    person_a[item] = person[item]
  end


  rankings = Array.new

  prefs.each_key do |other| #各アイテムごと
    totals = 0
    sim_sums = 0
    sim_counts = 0
    next if person_a.include?(other)#[MISSON:kaburi01]：テストアイテムを除く：重要　これがないと既に評価したアイテムが推薦される
    
    person_a.each do |item,val| #各ユーザ評価アイテムごと
      sim = __send__(similarity,prefs,item,other)#ユーザ評価アイテムと他の類似度　本当はまとめて事前計算しとけぇ…
      sim = sim.abs #ピアソンは1-~1までなので絶対値にしとかないと類似度が変になる！！
      totals+= sim*val
      sim_sums += sim
      next if sim == 0 #counts ここの２行で下のcounts発動
      sim_counts +=1
    end

    next if sim_sums == 0
    next if sim_counts <= 1 #counts ここで評価したアイテムと類似度が計算できたアイテムの数を調整, 1だとそのアイテムにすごく影響される
   rankings << [totals/sim_sums, other]
  end

  rankings.sort.reverse[0,$n_reco_item]
end


end
