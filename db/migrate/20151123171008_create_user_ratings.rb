class CreateUserRatings < ActiveRecord::Migration
  def change
    create_table :user_ratings do |t|
	    t.integer :item_ID
	    t.string  :user_ID

	    t.integer :rating_type
	    t.float :rating0
	            
		t.integer :r_point_pos
		t.string  :r_propaty_pos
		t.text    :r_reason_pos

		t.integer :r_point_mid
		t.string  :r_propaty_mid
		t.text    :r_reason_mid

		t.integer :r_point_neg
		t.string  :r_propaty_neg
		t.text    :r_reason_neg

		t.integer :r_point_oo
		t.string  :r_propaty_oo
		t.text    :r_reason_oo

		t.integer :r_point_xo
		t.string  :r_propaty_xo
		t.text    :r_reason_xo

		t.integer :r_point_xx
		t.string  :r_propaty_xx
		t.text    :r_reason_xx

		t.text    :rating_reason
		t.text    :comment
      t.timestamps
    end
  end
end
