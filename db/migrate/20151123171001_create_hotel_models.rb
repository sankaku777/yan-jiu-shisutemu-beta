class CreateHotelModels < ActiveRecord::Migration
  def change
    create_table :hotel_models do |t|
        t.integer :item_ID2
        t.integer :item_ID
        
        t.integer :propaty
        
        t.integer :total_oo
        t.integer :total_ox
        t.integer :total_xo
        t.integer :total_xx
        
        t.float :lift_oo
        t.float :lift_ox
        t.float :lift_xo
        t.float :lift_xx
      t.timestamps
    end
  end
end
