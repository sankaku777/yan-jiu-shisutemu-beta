class CreateHotelTitles < ActiveRecord::Migration
  def change
    create_table :hotel_titles do |t|
            t.integer :item_ID
            t.string :item_Name
            t.timestamps
    end
  end
end
