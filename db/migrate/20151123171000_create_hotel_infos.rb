class CreateHotelInfos < ActiveRecord::Migration
  def change
    create_table :hotel_infos do |t|
    	t.integer :item_ID
		t.string :item_Name

		t.float :rating0

		t.float :rating1
		t.float :rating2
		t.float :rating3
		t.float :rating4
		t.float :rating5
		t.float :rating6
		t.float :rating7

		t.string :pos
		t.string :mid
		t.string :neg

		t.string :item_type
		t.text :intro
		t.text :address
		t.text :access
		t.text 	:detail
		t.text :thumbnail
      t.timestamps
    end
  end
end
