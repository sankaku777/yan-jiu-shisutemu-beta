class CreateHotelRatings < ActiveRecord::Migration
  def change
    create_table :hotel_ratings do |t|
        t.integer :item_ID
        t.string :user_ID
        
        t.float :rating0
        
        t.float :rating1
        t.float :rating2
        t.float :rating3
        t.float :rating4
        t.float :rating5
        t.float :rating6
        t.float :rating7
        
        t.string :review_date
        t.text :review_title
        t.text :review
      t.timestamps
    end
  end
end
