# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:

#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


require "csv"

HotelTitle.destroy_all
HotelRating.destroy_all
HotelModel.destroy_all
HotelInfo.destroy_all

CSV.foreach('db/hotel_titles-fixed.csv') do |row|
  HotelTitle.create(:item_ID => row[0], :item_Name => row[1])
end

CSV.foreach('db/hotel_ratings-r.csv') do |row|
  HotelRating.create(:item_ID => row[1], :user_ID => row[2], :rating0 => row[3],:rating1 => row[4],:rating2 => row[5],:rating3 => row[6],:rating4 => row[7],:rating5 => row[8] ,:rating6 => row[9],:rating7 => row[10])
end

CSV.foreach('db/hotel_models.csv') do |row|
  HotelModel.create(:item_ID2 => row[1], :item_ID => row[2], :propaty => row[3],:total_oo => row[4],:total_ox => row[5],:total_xo => row[6],:total_xx => row[7],:lift_oo => row[8] ,:lift_ox => row[9],:lift_xo=> row[10],:lift_xx=> row[11])
end

CSV.foreach('db/hotel_infos.csv') do |row|
  HotelInfo.create(:item_ID => row[0], :item_Name => row[1], :rating0 => row[2],:rating1 => row[3],:rating2 => row[4],:rating3 => row[5],:rating4 => row[6],:rating5 => row[7] ,:rating6 => row[8],:rating7 => row[9],:pos => row[10],:mid => row[11],:neg=> row[12],:item_type => row[13],:intro => row[14],:address => row[15],:access => row[16],:detail => row[17],:thumbnail => row[18])
end