# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151123171008) do

  create_table "hotel_infos", force: true do |t|
    t.integer  "item_ID"
    t.string   "item_Name"
    t.float    "rating0",    limit: 24
    t.float    "rating1",    limit: 24
    t.float    "rating2",    limit: 24
    t.float    "rating3",    limit: 24
    t.float    "rating4",    limit: 24
    t.float    "rating5",    limit: 24
    t.float    "rating6",    limit: 24
    t.float    "rating7",    limit: 24
    t.string   "pos"
    t.string   "mid"
    t.string   "neg"
    t.string   "item_type"
    t.text     "intro"
    t.text     "address"
    t.text     "access"
    t.text     "detail"
    t.text     "thumbnail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotel_models", force: true do |t|
    t.integer  "item_ID2"
    t.integer  "item_ID"
    t.integer  "propaty"
    t.integer  "total_oo"
    t.integer  "total_ox"
    t.integer  "total_xo"
    t.integer  "total_xx"
    t.float    "lift_oo",    limit: 24
    t.float    "lift_ox",    limit: 24
    t.float    "lift_xo",    limit: 24
    t.float    "lift_xx",    limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotel_ratings", force: true do |t|
    t.integer  "item_ID"
    t.string   "user_ID"
    t.float    "rating0",      limit: 24
    t.float    "rating1",      limit: 24
    t.float    "rating2",      limit: 24
    t.float    "rating3",      limit: 24
    t.float    "rating4",      limit: 24
    t.float    "rating5",      limit: 24
    t.float    "rating6",      limit: 24
    t.float    "rating7",      limit: 24
    t.string   "review_date"
    t.text     "review_title"
    t.text     "review"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotel_titles", force: true do |t|
    t.integer  "item_ID"
    t.string   "item_Name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_profiles", force: true do |t|
    t.string   "user_ID"
    t.string   "pass"
    t.integer  "phase"
    t.text     "item01"
    t.text     "item02"
    t.text     "item03"
    t.string   "memo01"
    t.string   "memo02"
    t.string   "memo03"
    t.string   "memo04"
    t.string   "memo05"
    t.string   "memo06"
    t.string   "memo07"
    t.string   "memo08"
    t.string   "memo09"
    t.string   "memo10"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_ratings", force: true do |t|
    t.integer  "item_ID"
    t.string   "user_ID"
    t.integer  "rating_type"
    t.float    "rating0",       limit: 24
    t.integer  "r_point_pos"
    t.string   "r_propaty_pos"
    t.text     "r_reason_pos"
    t.integer  "r_point_mid"
    t.string   "r_propaty_mid"
    t.text     "r_reason_mid"
    t.integer  "r_point_neg"
    t.string   "r_propaty_neg"
    t.text     "r_reason_neg"
    t.integer  "r_point_oo"
    t.string   "r_propaty_oo"
    t.text     "r_reason_oo"
    t.integer  "r_point_xo"
    t.string   "r_propaty_xo"
    t.text     "r_reason_xo"
    t.integer  "r_point_xx"
    t.string   "r_propaty_xx"
    t.text     "r_reason_xx"
    t.text     "rating_reason"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
