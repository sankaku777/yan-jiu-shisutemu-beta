
#ホテルのサムネ画像を保存するクローラー
#実際使用するホテルIDを参照して(title_lists.csv)行う


# URLにアクセスするためのライブラリの読み込み
require 'open-uri'

# Nokogiriライブラリの読み込み
require 'nokogiri'

require 'csv'

#ホテルIDを元に画像urlを取得
def get_info(hotel_id)
    # スクレイピング先のURL
  url = 'http://4travel.jp/dm_hotel_each-'+(hotel_id).to_s+'.html'

  charset = nil
  html = open(url) do |f|
    charset = f.charset # 文字種別を取得
    f.read # htmlを読み込んで変数htmlに渡す
  end

  # htmlをパース(解析)してオブジェクトを生成
  doc = Nokogiri::HTML.parse(html, nil, charset)
  item_info = Array.new
  #サムネ画像
  item_info[0] = doc.xpath("//div[@class='main_photo_area photoMWrap'][1]").css('img').attribute('src').value

end

#画像urlとホテルIDを元に画像を保存
def save_file(url,hotel_id)
  filename = File.basename(url)

  dirName = "/Users/yammar/desktop/images/"
  filename = dirName + hotel_id.to_s + '.jpg'

  open(filename,'wb') do |file|
    open(url) do |data|
      file.write(data.read)
    end
  end

end



CSV.foreach("title_lists.csv") do |row|
  hotel_id = row[0]
  url = get_info(hotel_id)
  p hotel_id
  save_file(url,hotel_id)
  temp =  (4+rand(1..20).to_f/10.0)
  sleep(temp)
end



