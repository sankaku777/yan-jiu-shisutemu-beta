Rails.application.routes.draw do
  devise_for :users
  root to: "hotels#index"
    #deviseによる追加　ユーザ登録のあとはindexへ

  get 'hotels/index'#トップページ
  get 'hotels/manual'#手続きページ
  get 'hotels/list_test'#テストアイテム一覧
  get 'hotels/:type/show/:item_ID' => 'hotels#show'#ここで任意のパラメータを':XX'として登録しておくことでURLを自由に

  get 'hotels/ask_recommend'#推薦を行うか確認
  get 'hotels/list_recommend'#推薦アイテム一覧
  get 'hotels/create'#アイテム投稿後
  get 'hotels/result'#結果画面
  get 'hotels/result_show/:item_ID' => 'hotels#result_show'
  get 'hotels/interview'#アンケート画面

 
  post "hotels/create" => "hotels#create"#アイテム投稿
  post "hotels/result" => "hotels#result"#アンケート画面


# index
# |
#intro
#|
# list_test / show(:item_ID) / confirm_rating(:item_ID)
# |
# ask_recommend / cnmfirm_recommend
# |
# list_recommend/ show(:item_ID) /confirm_rating(:item_ID)
# |
# memo
# |
# アンケート



#いらないやつ=========
  # get 'users/show'
  # get 'users/index'
  # get 'users/show/?id=:item_ID' => 'users#show'
#=========

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
